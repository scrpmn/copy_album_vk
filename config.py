""" Parser of config.ini with sensitive information """
import configparser
import os
import sys

FILENAME = 'config.ini'
file = os.path.join(os.path.dirname(sys.argv[0]), FILENAME)  # pylint: disable=C0103
config = configparser.RawConfigParser()  # pylint: disable=C0103
config.read(file)
TEST_GROUP = int(config.get('Groups', 'Test'))
PRODUCTION_GROUP = int(config.get('Groups', 'Production'))
TOKENS = [i[1] for i in config.items('Tokens')]

PHOTOS_LIMIT_BY_REQUEST = 1000
PHOTOS_LIMIT_BY_TIME = 2000  # max photos for 6 hours
